const fs = require('fs');

const data = fs.readFileSync('./JSON_INFO.json');

const Json = JSON.parse(data);

const Nightmare = require('nightmare');

const nightmare =  Nightmare({ show: true });

asyncForEach(Json,async(e,index)=>{
    await search(e.url_target);
})

// Json.forEach(element => {
//     return search(element.url_target);
// });


async function search (url){
   await nightmare.goto(url)
    .wait(3000)
    .evaluate(async function(){
    //    const precio = $('.important').text();
    let object ={};
    await $('.m_property_info_details').children('dl').each(function(index){
        return object[$(this).children('dt').text().replace(/\t|\n/g,'')] = $(this).children('dd').text().replace(/\t|\n/g,'');
    })

    object.imagenes = [];

   await $('.serviceImg').each(function(){
       return object.imagenes.push($(this).attr('src'))
    })


    return object;
    })
    .then(result=>{
        console.log(result);
    })
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
}
  