
const jquery = require('jquery');
const Nightmare = require('nightmare');
const path = require('path')
const nightmare =  Nightmare({ show: true });

const fs = require('fs');
// https://www.fincaraiz.com.co/casas/venta/bogota/?ad=30|4||||1||9|||67|3630001|||||||||||||||||||1||griddate%20desc||||||


// asyncForEach(JsonData,async function(item,index){
//   await nightmare.goto(item.url_targets)
//         .wait(3000)
//         .evaluate(function(){
//           const products = [];

//           $('#divAdverts').children('ul').each(function(){
//            const link = `"${$(this).attr('onclick')}"`.replace('javascript:window.location=','');
//            products.push({property_link:link});
//           })

//           return products;

//         })
// })
// .then(result=>{
// console.log(result);
// })

async function Scraper (url,index=1){
 
  await nightmare.goto(url)
    .wait(2000)
    .evaluate(async function(){
      const products = [];
      await $('#divAdverts').children('ul').each( async function(){
      const link = $(this).children('li').attr('onclick').replace('javascript:window.location=','');
      if(link == undefined) return;
      products.push({property_link:link});
      });


     return products;
     
    })
    .then(results=>{  

      fs.readFile('./propiedadeslinkfinca.json', async (err,data)=>{
        if(err) console.error('Error: ',err);

        let json = JSON.parse(data);
        const productos = json.concat(results)
        return fs.writeFile('./propiedadeslinkfinca.json',JSON.stringify(productos),err=>{if(err) console.error(err)});

      })
      
      if(index > 10) return;
      Scraper(`https://www.fincaraiz.com.co/casas/venta/bogota/?ad=30|${index++}||||1||9|||67|3630001||||||||||||||||1|||1||griddate%20desc||||-1||`,index++);
    })


}



Scraper(`https://www.fincaraiz.com.co/casas/venta/bogota/?ad=30|1||||1||9|||67|3630001||||||||||||||||1|||1||griddate%20desc||||-1||`,1);


async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
