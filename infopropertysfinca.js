const jquery = require('jquery');
const Nightmare = require('nightmare');
const path = require('path')
const nightmare =  Nightmare({ show: true });

const fs = require('fs');

const dbConnection = require('./dbConnection');
const ConnectionBD = dbConnection();

const stringData = fs.readFileSync('./propiedadeslinkfinca.json',function(err,data){
    if(err) console.error(err);
    return  JSON.stringify(data); 
})

// console.log(stringData);

// const jsontesting = [{"property_link":"/casa-en-venta/bogota/jose_antonio_galan-det-5229994.aspx"},
// {"property_link":"/casa-en-venta/bogota/villa_mayor-det-5205060.aspx"},
// {"property_link":"/casa-en-venta/bogota/zona_sur-det-5251297.aspx"},
// {"property_link":"/casa-en-venta/bogota/iberia-det-4991750.aspx"}];


// const jsonString = JSON.stringify(jsontesting);
// const Data = JSON.parse(jsonString);

const Data = JSON.parse(stringData);
//https://www.fincaraiz.com.co/casa-en-venta/bogota/mirandela-det-5245247.aspx
//lblPhoneContact2
asyncForEach(Data,async (link,index)=>{

await nightmare.goto('https://www.fincaraiz.com.co'+link.property_link)
    .wait(2000)
    .click('#lblPhoneContact2')
    .wait(2000)
    .evaluate(async function(){

        const information = {};

        // information.titulo = $('div .box').children('h1').text();
        // information.precio = $('div .price').children('h2').text();
        // information.area_construccion = $('div .features').children('.advertSurface').text().replace(/\n/g,'').replace(/\ /g,'');
        // information.habitaciones = $('div .features').children('.advertRooms').text().replace(/\n/g,'').replace(/\ /g,'');
        // information.banios = $('div .features').children('.advertBaths').text().replace(/\n/g,'').replace(/\ /g,'');
        // information.garages = $('div .features').children('.advertGarages').text().replace(/\n/g,'').replace(/\ /g,'');

        // information.area_privada= document.querySelector('ul.boxcube').children[0].textContent.replace(/\n/g,'').replace(/\ /g,'');
        // information.estrato= document.querySelector('ul.boxcube').children[3].textContent.replace(/\n/g,'').replace(/\ /g,'');

        // information.descripcion= document.querySelector('div.boxcube p').textContent;

        // information.coordenadas ={
        //   lat:MapFR.getLatitude(),
        //   lon:MapFR.getLongitude()
        // }
        const foto_telefono = $('#imgPhone1divPhone').attr('src') ? $('#imgPhone1divPhone').attr('src') : $('#imgClickToCalldivPhone').attr('src');

        information.phone_foto = `https://www.fincaraiz.com.co${foto_telefono}`;
        information.titulo = sfAdvert.Title;
        information.precio = parseInt(sfAdvert.Price, 10);
        information.area_construccion = sfAdvert.Area;
        information.habitaciones = parseInt(sfAdvert.Rooms, 10);
        information.banios = parseInt(sfAdvert.Baths, 10);
        information.garages = sfAdvert.Garages ? parseInt(sfAdvert.Garages, 10)  : 0;
        information.estrato =parseInt(sfAdvert.Stratum, 10);
        information.descripcion = sfAdvert.Description;
        information.coordenadas = sfAdvert.Latitude !='0' && sfAdvert.Longitude !='0' ? `${sfAdvert.Latitude},${sfAdvert.Longitude}` : '';

        information.fotos = GetPhotosArray().length > 7 ? GetPhotosArray().splice(0,7) : GetPhotosArray();

        return information;
    })
    .then(response=>{

        const fotos = response.fotos;
        delete response.fotos;
        return ConnectionBD.query(`INSERT INTO propiedades_finca_raiz SET ?`,response,(err,result)=>{
          if(err) return console.error(err);

          console.log(response.titulo);

          fotos.forEach(function(item) {
            if(item == '#ClickToCall#') return;
            ConnectionBD.query(`INSERT INTO finca_raiz_fotos SET ?`,{idfinca_property:result.insertId,fotos:item},function(err,result){
               if(err) return console.error(err);
            })
          });

        })
    })
    .catch(e=>console.error(e))
})




async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index],index,array);
  }
}
  